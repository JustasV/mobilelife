﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main()
        {
            ProducerClient client = new ProducerClient();
            string root = new FileInfo(Assembly.GetExecutingAssembly().Location).FullName;
            string FilePath = root.Substring(0, root.IndexOf("\\bin")) + "\\PvzXML.xml";
            if (File.Exists(FilePath))
            {
                client.Import(FilePath);
            }

            client.AddTax(1, 1, DateTime.Now.Date, (decimal)0.1);
            client.AddTax(1, 2, DateTime.Now.AddMonths(1).Date, (decimal)0.2);
            client.AddTax(2, 3, DateTime.Now.Date, (decimal)0.3);
            client.AddTax(2, 4, DateTime.Now.AddMonths(1).Date, (decimal)0.4);
            client.AddTax(3, 4, DateTime.Now.AddMonths(1).Date, (decimal)0.2);
            

            decimal val1 = client.SearchTax(2, DateTime.Now.AddMonths(2).Date);
            decimal val2 = client.SearchTax(1, DateTime.Now.Date);
            decimal val3 = client.SearchTax(1, DateTime.Now.Date);

            //client.DeleteTax(1);

            client.Close();
        }
    }
}
