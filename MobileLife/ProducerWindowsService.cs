﻿using MobileLife.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace MobileLife
{
    public class ProducerWindowsService : ServiceBase
    {
        public ServiceHost serviceHost = null;
        public ProducerWindowsService()
        {
            ServiceName = Globals.ServiceName;
        }

        public static void Main()
        {
            ServiceBase.Run(new ProducerWindowsService());
        }
        
        protected override void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }
            
            serviceHost = new ServiceHost(typeof(ProducerService));
            
            serviceHost.Open();
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }
        }
    }
}
