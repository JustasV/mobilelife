﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MobileLife.Models
{
    class Tax
    {
        public int TaxID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Value { get; set; }
        public DateTime DateCreated { get; set; }
        
        public int TaxTypeID { get; set; }
        [ForeignKey("TaxTypeID")]
        public TaxType TaxType { get; set; }

        public int MunicipalityID { get; set; }
        [ForeignKey("MunicipalityID")]
        public Municipality Municipality { get; set; }
    }
}
