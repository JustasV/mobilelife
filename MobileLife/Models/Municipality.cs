﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MobileLife.Models
{
    public class Municipality
    {
        public int MunicipalityID { get; set; }

        [MaxLength(50)]
        public string MunicipalityName { get; set; }

        ICollection<Tax> Taxes { get; set; }
    }
}
