﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace MobileLife.Models
{
    class MobileLifeDb : DbContext
    {
        public MobileLifeDb() : base("name=MobileLifeConnectionString")
        {

        }

        public DbSet<TaxType> TaxTypes { get; set; }
        public DbSet<Tax> Taxes { get; set; }
        public DbSet<Municipality> Municipalities { get; set; }
        public DbSet<ErrorLog> ErrorLogs { get; set; }
    }
}
