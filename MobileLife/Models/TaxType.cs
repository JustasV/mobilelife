﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace MobileLife.Models
{
    class TaxType
    {
        public int TaxTypeID { get; set; }

        [MaxLength(50)]
        public string Description { get; set; }

        ICollection<Tax> Taxes { get; set; }
    }
}
