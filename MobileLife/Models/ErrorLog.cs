﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MobileLife.Models
{
    public class ErrorLog
    {
        public int ErrorLogID { get; set; }

        [MaxLength(500)]
        public string Message { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
