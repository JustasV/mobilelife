﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileLife.Class
{
    static class Globals
    {
        public static string ServiceName = "Mobile Life Producer Service";

        public enum TaxType
        {
            Daily = 1,
            Weekly,
            Monthly,
            Yearly
        }
    }
}
