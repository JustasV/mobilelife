﻿using System;
using System.Collections.Generic;
using System.Linq;
using MobileLife.Interface;
using MobileLife.Models;
using MobileLife.Class.XML;

namespace MobileLife.Class
{
    public class ProducerService : IProducer
    {
        MobileLifeDb db;
        //Should be Byte Array, but will work for test project
        bool IProducer.Import(string path)
        {
            try
            {
                db = new MobileLifeDb();
                List<Municipality_Serializable> municipalities = ProducerServiceHelper.ReadXML(path);
                if (municipalities != null)
                {
                    foreach (Municipality_Serializable item in municipalities)
                    {
                        if (!db.Municipalities.Any(m => m.MunicipalityName.Trim().ToLower() == item.MunicipalityName.Trim().ToLower()))
                        {
                            db.Municipalities.Add(new Municipality()
                            {
                                MunicipalityName = item.MunicipalityName
                            });
                        }
                    }

                    db.SaveChanges();
                    return true;
                }

                //error loging
                db.ErrorLogs.Add(new ErrorLog()
                {
                    Message = "Serialisation Failure",
                    DateCreated = DateTime.Now
                });
                db.SaveChanges();
                return false;
            }
            catch (Exception Ex)
            {
                //ErrorLoging
                db = new MobileLifeDb();
                db.ErrorLogs.Add(new ErrorLog()
                {
                    Message = Ex.Message,
                    DateCreated = DateTime.Now
                });
                db.SaveChanges();
                return false;
            }
        }

        public bool AddTax(int MunicipalityID, int TaxTypeID, DateTime StartDate, decimal Value)
        {
            try
            {
                db = new MobileLifeDb();
                Tax tax = new Tax();
                tax.MunicipalityID = MunicipalityID;
                tax.TaxTypeID = TaxTypeID;
                bool TypeExists = true;
                switch (TaxTypeID)
                {
                    case (int)Globals.TaxType.Daily:
                        tax.StartDate = StartDate.Date;
                        tax.EndDate = StartDate.Date;
                        break;
                    case (int)Globals.TaxType.Weekly:
                        tax.StartDate = StartDate.Date;
                        tax.EndDate = StartDate.AddDays(7).Date;
                        break;
                    case (int)Globals.TaxType.Monthly:
                        tax.StartDate = StartDate.Date;
                        tax.EndDate = StartDate.AddMonths(1).Date;
                        break;
                    case (int)Globals.TaxType.Yearly:
                        tax.StartDate = StartDate.Date;
                        tax.EndDate = StartDate.AddYears(1).Date;
                        break;
                    default:
                        TypeExists = false;
                        break;
                }

                tax.Value = Value;

                if (!TypeExists)
                {
                    //ErrorLoging
                    db = new MobileLifeDb();
                    db.ErrorLogs.Add(new ErrorLog()
                    {
                        Message = "Failure adding tax due to non-existant Tax Type",
                        DateCreated = DateTime.Now
                    });
                    db.SaveChanges();
                    return false;
                }

                tax.DateCreated = DateTime.Now;
                db.Taxes.Add(tax);
                db.SaveChanges();
                return true;
            }
            catch (Exception Ex)
            {
                //ErrorLoging
                db = new MobileLifeDb();
                db.ErrorLogs.Add(new ErrorLog()
                {
                    Message = Ex.Message,
                    DateCreated = DateTime.Now
                });
                db.SaveChanges();
                return false;
            }
        }

        /*We look for taxes in database by date
          First we check for daily taxes, then weekly, monthly, yearly.
          Pretty sure there should be functionality to return multiple taxes by the same date,
          but since it is not mentioned in NET CodingTask.pdf... Same goes for error logging
        */
        public decimal SearchTax(int MunicipalityID, DateTime date)
        {
            db = new MobileLifeDb();
            List<Tax> taxList = db.Taxes.Where(t => t.MunicipalityID == MunicipalityID
                                                 && t.StartDate <= date
                                                 && t.EndDate >= date)
                                        .ToList();

            if (taxList.Any(t => t.TaxTypeID == (int)Globals.TaxType.Daily))
            {
                return taxList.Where(t => t.TaxTypeID == (int)Globals.TaxType.Daily)
                              .Select(t => t.Value)
                              .FirstOrDefault();
            }

            if (taxList.Any(t => t.TaxTypeID == (int)Globals.TaxType.Weekly))
            {
                return taxList.Where(t => t.TaxTypeID == (int)Globals.TaxType.Weekly)
                              .Select(t => t.Value)
                              .FirstOrDefault();
            }

            if (taxList.Any(t => t.TaxTypeID == (int)Globals.TaxType.Monthly))
            {
                return taxList.Where(t => t.TaxTypeID == (int)Globals.TaxType.Monthly)
                              .Select(t => t.Value)
                              .FirstOrDefault();
            }

            if (taxList.Any(t => t.TaxTypeID == (int)Globals.TaxType.Yearly))
            {
                return taxList.Where(t => t.TaxTypeID == (int)Globals.TaxType.Yearly)
                              .Select(t => t.Value)
                              .FirstOrDefault();
            }
            return 0;
        }

        public bool DeleteTax(int TaxID)
        {
            try
            {
                db = new MobileLifeDb();
                Tax tax = db.Taxes.Where(tt => tt.TaxID == TaxID).FirstOrDefault();
                if (tax != null)
                {
                    db.Taxes.Remove(tax);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    //ErrorLoging, record does not exist
                    db = new MobileLifeDb();
                    db.ErrorLogs.Add(new ErrorLog()
                    {
                        Message = "Delete action could not find record",
                        DateCreated = DateTime.Now
                    });
                    db.SaveChanges();
                    return false;
                }
            }
            catch (Exception Ex)
            {
                //ErrorLoging
                db = new MobileLifeDb();
                db.ErrorLogs.Add(new ErrorLog()
                {
                    Message = Ex.Message,
                    DateCreated = DateTime.Now
                });
                db.SaveChanges();
                return false;
            }
        }
    }
}
