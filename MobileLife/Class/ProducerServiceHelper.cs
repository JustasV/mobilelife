﻿using MobileLife.Class.XML;
using MobileLife.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace MobileLife.Class
{
    public static class ProducerServiceHelper
    {
        public static List<Municipality_Serializable> ReadXML(string path)
        {
            try
            {
                List < Municipality_Serializable> municipalities = null;
                if (File.Exists(path))
                {
                    try
                    {
                        XDocument xd1 = new XDocument();
                        xd1 = XDocument.Load(path);
                    }
                    catch (XmlException Ex)
                    {
                        //Error logging, invalid xml file
                        MobileLifeDb db = new MobileLifeDb();
                        db.ErrorLogs.Add(new ErrorLog()
                        {
                            Message = Ex.Message,
                            DateCreated = DateTime.Now
                        });
                        db.SaveChanges();
                        return null;
                    }

                    XmlSerializer serializer = new XmlSerializer(typeof(List<Municipality_Serializable>), new XmlRootAttribute("MunicipalityCollection"));

                    using (StreamReader reader = new StreamReader(path))
                    {
                        municipalities = (List<Municipality_Serializable>)serializer.Deserialize(reader);
                    }

                    return municipalities;
                }
            }
            catch (Exception Ex)
            {
                MobileLifeDb db = new MobileLifeDb();
                db.ErrorLogs.Add(new ErrorLog()
                {
                    Message = Ex.Message,
                    DateCreated = DateTime.Now
                });
                db.SaveChanges();
                return null;
            }
            return null;
        }
    }
}
