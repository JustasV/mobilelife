﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MobileLife.Class.XML
{
    [Serializable()]
    [XmlType("Municipality")]
    public class Municipality_Serializable
    {
        [System.Xml.Serialization.XmlElement("MunicipalityName")]
        public string MunicipalityName { get; set; }
    }
}
