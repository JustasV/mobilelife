namespace MobileLife.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ErrorLogging : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ErrorLogs",
                c => new
                    {
                        ErrorLogID = c.Int(nullable: false, identity: true),
                        Message = c.String(maxLength: 500),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ErrorLogID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ErrorLogs");
        }
    }
}
