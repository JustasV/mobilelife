namespace MobileLife.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Municipalities",
                c => new
                    {
                        MunicipalityID = c.Int(nullable: false, identity: true),
                        MunicipalityName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.MunicipalityID);
            
            CreateTable(
                "dbo.Taxes",
                c => new
                    {
                        TaxID = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TaxTypeID = c.Int(nullable: false),
                        MunicipalityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TaxID)
                .ForeignKey("dbo.Municipalities", t => t.MunicipalityID, cascadeDelete: true)
                .ForeignKey("dbo.TaxTypes", t => t.TaxTypeID, cascadeDelete: true)
                .Index(t => t.TaxTypeID)
                .Index(t => t.MunicipalityID);
            
            CreateTable(
                "dbo.TaxTypes",
                c => new
                    {
                        TaxTypeID = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.TaxTypeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Taxes", "TaxTypeID", "dbo.TaxTypes");
            DropForeignKey("dbo.Taxes", "MunicipalityID", "dbo.Municipalities");
            DropIndex("dbo.Taxes", new[] { "MunicipalityID" });
            DropIndex("dbo.Taxes", new[] { "TaxTypeID" });
            DropTable("dbo.TaxTypes");
            DropTable("dbo.Taxes");
            DropTable("dbo.Municipalities");
        }
    }
}
