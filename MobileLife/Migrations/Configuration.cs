namespace MobileLife.Migrations
{
    using MobileLife.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MobileLife.Models.MobileLifeDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MobileLife.Models.MobileLifeDb context)
        {
            context.TaxTypes.AddOrUpdate(TT => TT.TaxTypeID,
                new TaxType() { TaxTypeID = 1, Description = "Daily" },
                new TaxType() { TaxTypeID = 2, Description = "Weekly" },
                new TaxType() { TaxTypeID = 3, Description = "Monthly" },
                new TaxType() { TaxTypeID = 4, Description = "Yearly" }
                );
        }
    }
}
