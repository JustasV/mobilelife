﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MobileLife.Interface
{
    // Define a service contract.
    [ServiceContract(Namespace = "http://MobileLife.Class")]
    public interface IProducer
    {
        [OperationContract]
        bool Import(string Path);
        [OperationContract]
        bool AddTax(int MunicipalityID, int TaxTypeID, DateTime Date, decimal Value);
        [OperationContract]
        decimal SearchTax(int MunicipalityID, DateTime date);
        [OperationContract]
        bool DeleteTax(int TaxID);
    }
}
